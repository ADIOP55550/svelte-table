

const columnNames: Map<string, string> = new Map([
	['begginingDate', 'Data rozpoczęcia'],
	['gameName', 'Nazwa gry'],
	['type', 'Typ gry'],
	['companyName', 'Nazwa firmy'],
	['currentRound', 'Aktualna runda'],
	['harmonogram', 'Harmonogram rund'],
	['certificate', 'Certyfikat'],
	['enterGame', 'Wejście do gry'],
]);

// In real app there should be some real i18n library
export function resolveColumnName(columnId: string): string | undefined {
	return columnNames.get(columnId);
}

export function resolveColumnInfo(_columnId: string): string | undefined {
	return "Lorem ipsum dolor sit amet.";
}

